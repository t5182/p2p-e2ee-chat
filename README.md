# P2P E2EE Chat
This project is an exploratory P2P E2EE Chat app with GitLab CI. It uses 
built-in Java tools to send and receive messages to other peers
on the local network. Messages are encrypted using Google Tink's hybrid encryption
which combines public-key encryption and a symmetric key encryption.

## Requirements
- Google Tink 1.6.1
- Docker
- Maven 3.8
- OpenJDK 11
- Ubuntu 20.04

## Demo
https://www.youtube.com/watch?v=adD_sbzGSjc

## Step-by-step guide with Docker

1. Open a command prompt in directory where Dockerfile is saved
2. Enter in command prompt "docker build -t whalepizzashark-p2p:latest ."
and then "docker-compose -f .\docker-compose-p2p.yml up -d"
3. Enter in command prompt 
"docker exec -it peer1 bash -c "java -cp target/p2p-e2ee-chat-1.0.jar com.tamagoyaki.chat.Peer""
4. Enter in a new command prompt
"docker exec -it peer2 bash -c "java -cp target/p2p-e2ee-chat-1.0.jar com.tamagoyaki.chat.Peer""
5. Enter username at prompt
6. Enter selection at menu: 1 to connect, 2 to send a message, and 3 to exit
![](menu.png)
7. If "Connect to Peer" is selected, a local network scan will take place if no
connections have been made
8. Enter option number for IP or the full IP of choice.            
![](networkScan.png)
9. If "Send message to Peer" is selected, a list of connected peers will be shown
for selection. Select peer of choice using option number or IP address
10. Enter message                   
![](message.png)

## Step-by-step guide without Docker

1. Build and run Peer.java in your Java IDE. To run multiple peers on the same IDE
SERVER_PORT and PEER_PORT must be changed.
2. Enter selection at menu: 1 to connect, 2 to send a message, and 3 to exit
   ![](menu.png)
3. If "Connect to Peer" is selected, a local network scan will take place if no
   connections have been made
4. Enter option number for IP or the full IP of choice.            
   ![](networkScan.png)
5. If "Send message to Peer" is selected, a list of connected peers will be shown
   for selection. Select peer of choice using option number or IP address
6. Enter message                   
   ![](message.png)
