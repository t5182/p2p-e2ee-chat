FROM ubuntu:20.04

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y software-properties-common && \
    apt-get update && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y maven gradle git vim build-essential && \
    apt-get clean

RUN mkdir /p2p_chat

COPY src /p2p_chat/src
COPY pom.xml /p2p_chat/
COPY README.md /p2p_chat/

WORKDIR /p2p_chat

RUN cd /p2p_chat && mvn package
