package com.tamagoyaki.chat;

import com.google.crypto.tink.CleartextKeysetHandle;
import com.google.crypto.tink.HybridDecrypt;
import com.google.crypto.tink.HybridEncrypt;
import com.google.crypto.tink.JsonKeysetReader;
import com.google.crypto.tink.JsonKeysetWriter;
import com.google.crypto.tink.KeyTemplates;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.hybrid.HybridConfig;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * The Peer class allows users on the same local network to chat
 * with each other using encrypted messages.
 *
 * @author Henry Nguyen
 *
 * @version 1.0
 */
public class Peer {

  private static ServerSocket serverSocket;
  private static final int SERVER_PORT = 8000;
  private static final int PEER_PORT = 8000;
  private static Map<String, Socket> clientMap = new LinkedHashMap<>();
  private static Map<String, String> connectMap = new LinkedHashMap<>();
  private static Map<String, String> recipientMap = new LinkedHashMap<>();
  private static Map<String, String> usernameMap = new HashMap<>();
  private static Map<String, KeysetHandle> peerPublicKeyMap = new HashMap<>();
  private static ArrayList<String> ipArrayList = new ArrayList<>();
  private static KeysetHandle privateKeysetHandle;
  private static String publicKey;
  private static String username;

  /**
   * The ClientHandle class handles each peer that is
   * connected to the user. It handles receiving
   * messages, username exchange, and public key exchange.
   *
   * @author Henry Nguyen
   *
   * @version 1.0
   */
  private static class ClientHandler extends Thread {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private String clientIpAddress;
    private String peerUsername;
    private static final int SOCKET_TIMEOUT_IN_MILLISECONDS = 15000;

    public ClientHandler(Socket socket, String clientIpAddress) {
      this.clientSocket = socket;
      this.clientIpAddress = clientIpAddress;
    }

    @Override
    public void run() {
      try {
        clientSocket.setSoTimeout(SOCKET_TIMEOUT_IN_MILLISECONDS);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // If user is not already connected to the client, connect to them
        if (!clientMap.containsKey(clientIpAddress)) {
          connectToPeer(clientIpAddress);
        }

        Socket reverseSocket = clientMap.get(clientIpAddress);
        out = new PrintWriter(reverseSocket.getOutputStream(), true);

        // Send the user's username and receive the peer's username
        out.println(username);
        do {
          peerUsername = in.readLine();
        } while (peerUsername == null || peerUsername.length() == 0);
        usernameMap.put(clientIpAddress, peerUsername);

        // Send the user's public key and receive the peer's public key
        out.println(publicKey);
        String peerPublicKey;
        do {
          peerPublicKey = in.readLine();
        } while (peerPublicKey == null || peerPublicKey.length() == 0);

        // Convert the peer's public key from string to KeysetHandle and put in the key map
        KeysetHandle peerPublicKeyHandle = CleartextKeysetHandle.read(JsonKeysetReader.withString(peerPublicKey));
        peerPublicKeyMap.put(clientIpAddress, peerPublicKeyHandle);

        HybridDecrypt hybridDecrypt = privateKeysetHandle.getPrimitive(HybridDecrypt.class);

        String input;
        do {
          input = in.readLine();
          if (input != null && input.length() != 0) {
            if (input.equals("Alive")) { // Heartbeat message
              continue;
            }
            byte[] ciphertext = Base64.getDecoder().decode(input);
            byte[] plaintext = hybridDecrypt.decrypt(ciphertext, null);
            input = new String(plaintext, StandardCharsets.ISO_8859_1);
            System.out.println(peerUsername + ": " + input);
          }
        } while (input != null); // A null is received when a peer disconnects

      } catch (IOException | GeneralSecurityException e) {
        System.out.println("Lost connection with " + peerUsername);
      } finally {
        try {
          in.close();
          out.close();
          clientSocket.close();

          clientMap.remove(clientIpAddress);
          peerPublicKeyMap.remove(clientIpAddress);
          connectMap.values().remove(clientIpAddress);
          recipientMap.values().remove(clientIpAddress);
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * The ServerHandle class handles peers connecting to the user
   * by accepting the connection and creating a client handler for them.
   *
   * @author Henry Nguyen
   *
   * @version 1.0
   */
  private static class ServerHandler implements Runnable {

    private boolean isServerRunning;

    public ServerHandler() {
      this.isServerRunning = true;
    }

    @Override
    public void run() {
      try {
        serverSocket = new ServerSocket(SERVER_PORT);
        while (isServerRunning) {
          Socket clientSocket = serverSocket.accept();
          new ClientHandler(clientSocket, clientSocket.getInetAddress().getHostAddress()).start();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    public void stop() {
      isServerRunning = false;
    }
  }

  public static void main(String[] args) {

    hybridEncryptionSetup();

    Scanner input = new Scanner(System.in);
    System.out.print("Enter username please: ");
    username = input.nextLine();

    new Thread(new ServerHandler()).start();
    startSendingHeartbeatMessagesToPeers();

    while (true) {
      printMenu();

      String choice = input.nextLine();

      switch (choice) {
        case "1":
          checkIfLocalNetworkScanIsNeeded();

          // Print out ipList with a number to make it easier to select
          System.out.println("Option | Address");
          System.out.println("----------------");
          int counter = 1;
          for (String IP : ipArrayList) {
            System.out.println(counter + " | " + IP);
            connectMap.put(String.valueOf(counter), IP);
            counter++;
          }
          System.out.println("0 | Connect all");
          System.out.print("Enter IP: ");
          String ipAddress = input.nextLine();

          // Connect to all detected IP addresses on the local network
          if (ipAddress.equals("0")) {
            validateAndConnectToAllDetectedIPs(counter);
            break;
          }

          // If the option selected maps to an IP Address, get and set
          if (connectMap.containsKey(ipAddress)) {
            ipAddress = connectMap.get(ipAddress);
          }

          validateAndConnectToIP(ipAddress);

          try {
            Thread.sleep(2000); // Wait for other peers to connect back, before menu reprints
          } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
          }
          break;
        case "2":
          selectPeerAndSendMessage();
          break;
        case "3":
          System.exit(0);
        default:
          System.out.println("Invalid input, please try again!");
          break;
      }
    }
  }

  private static void printMenu() {
    System.out.print(
        "--------------------------\n"
            + "Welcome to Tamagoyaki\n"
            + "1: Connect to Peer\n"
            + "2: Send message to Peer\n"
            + "3: Exit\n"
            + "--------------------------\n\n"
            + "Enter number for selection: "
    );
  }

  private static void hybridEncryptionSetup() {
    try {
      HybridConfig.register();

      privateKeysetHandle = KeysetHandle.generateNew(KeyTemplates.get("ECIES_P256_COMPRESSED_HKDF_HMAC_SHA256_AES128_GCM"));
      KeysetHandle publicKeysetHandle = privateKeysetHandle.getPublicKeysetHandle();
      publicKey = keyToString(publicKeysetHandle);
    } catch (GeneralSecurityException e) {
      e.printStackTrace();
    }
  }

  private static void checkIfLocalNetworkScanIsNeeded() {
    if (ipArrayList.isEmpty() && clientMap.isEmpty()) {
      localNetworkScan();
    } else {
      System.out.println("Scan local network? For yes enter y: ");
      Scanner input = new Scanner(System.in);
      String choice = input.nextLine();
      if (choice.equalsIgnoreCase("y")) {
        localNetworkScan();
      }
    }
  }

  private static void localNetworkScan() {
    try {
      String localhost = InetAddress.getLocalHost().getHostAddress();
      String localNetwork = localhost.substring(0, localhost.lastIndexOf(".") + 1);
      ExecutorService executor = Executors.newFixedThreadPool(20);

      System.out.println("Scanning local network");
      for (int i = 2; i < 255; i++) {
        String ip = localNetwork + i;
        executor.execute(() -> {
          try {
            if (InetAddress.getByName(ip).isReachable(500) && !ip.equals(localhost) && !ipArrayList.contains(ip)) {
              ipArrayList.add(ip);
            }
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
      }
      executor.shutdown();
      if (!executor.awaitTermination(8, TimeUnit.SECONDS)) {
        System.err.println("Timed out scanning local network");
      }

      ipArrayList.sort((o1, o2) -> {
        Integer o1Suffix = Integer.parseInt(o1.split("\\.")[3]);
        Integer o2Suffix = Integer.parseInt(o2.split("\\.")[3]);
        return o1Suffix.compareTo(o2Suffix);
      });

    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
      Thread.currentThread().interrupt();
    }
  }

  private static void connectToPeer(String address) {
    try {
      Socket clientSocket = new Socket(address, PEER_PORT);
      clientMap.put(address, clientSocket);
      System.out.println("Successfully Connected");
    } catch (IOException e) {
      System.out.println("There was an error connecting. Please check entered IP");
    }
  }

  private static boolean validateIP(String ip) {
    String pattern = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
    return ip.matches(pattern);
  }

  private static String keyToString(KeysetHandle keysetHandle) {
    ByteArrayOutputStream keyByte = new ByteArrayOutputStream();
    try {
      CleartextKeysetHandle.write(keysetHandle, JsonKeysetWriter.withOutputStream(keyByte));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return keyByte.toString();
  }

  private static void validateAndConnectToIP(String ipAddress) {
    if (validateIP(ipAddress)) {
      // If the user is not already connected to the client, connect to them
      if (!clientMap.containsKey(ipAddress)) {
        connectToPeer(ipAddress);
      } else {
        System.out.println("Already connected to " + ipAddress);
      }
    } else {
      System.out.println("The option is not valid!");
    }
  }

  private static void validateAndConnectToAllDetectedIPs(int counter) {
    String ipAddress;
    for (int i = 1; i < counter; i++) {
      ipAddress = connectMap.get(String.valueOf(i));
      if (validateIP(ipAddress) && !clientMap.containsKey(ipAddress)) {
        connectToPeer(ipAddress);
      }
    }
  }

  private static void selectPeerAndSendMessage() {
    // Print out the connected peers with a number to make it easier to select
    System.out.println("Option | Username | Address");
    System.out.println("---------------------------");
    int counter = 1;
    for (String key : clientMap.keySet()) {
      System.out.println(counter + " | " + usernameMap.get(key) + " | " + key);
      recipientMap.put(String.valueOf(counter), key);
      counter++;
    }
    System.out.println("0 | Message all");
    System.out.print("Enter recipient: ");
    try {
      Scanner read = new Scanner(System.in);
      String host = read.nextLine();

      if (recipientMap.containsKey(host)) {
        host = recipientMap.get(host);
      }

      if (clientMap.containsKey(host)) {
        System.out.print("Enter message: ");
        String message = read.nextLine();
        Socket clientSocket = clientMap.get(host);

        KeysetHandle peerPublicKeyHandle = peerPublicKeyMap.get(host);
        HybridEncrypt hybridEncrypt = peerPublicKeyHandle.getPrimitive(HybridEncrypt.class);
        byte[] ciphertext = hybridEncrypt.encrypt((message.getBytes(StandardCharsets.ISO_8859_1)), null);
        // Encode message to send without any byte changes
        String encodedMessage = Base64.getEncoder().encodeToString(ciphertext);

        PrintWriter toPeer = new PrintWriter(clientSocket.getOutputStream(), true);
        toPeer.println(encodedMessage);

      }

      // Message all peers
      if (host.equals("0")) {
        System.out.print("Enter message: ");
        String message = read.nextLine();
        for (int i = 1; i < counter; i++) {
          host = recipientMap.get(String.valueOf(i));
          Socket clientSocket = clientMap.get(host);

          KeysetHandle peerPublicKeyHandle = peerPublicKeyMap.get(host);
          HybridEncrypt hybridEncrypt = peerPublicKeyHandle.getPrimitive(HybridEncrypt.class);
          byte[] ciphertext = hybridEncrypt.encrypt(message.getBytes(StandardCharsets.ISO_8859_1), null);
          String encodedMessage = Base64.getEncoder().encodeToString(ciphertext);

          PrintWriter toPeer = new PrintWriter(clientSocket.getOutputStream(), true);
          toPeer.println(encodedMessage);

        }
      }
    } catch (Exception e) {
      System.out.println("There was an error sending the message!");
    }
  }

  private static void startSendingHeartbeatMessagesToPeers() {
    new Thread(() -> {
      try {
        while (true) {
          for (Map.Entry<String, Socket> entry : clientMap.entrySet()) {
            // Start sending heartbeat messages after key exchange is complete
            if(peerPublicKeyMap.containsKey(entry.getKey())) {
              Socket heartbeatSocket = entry.getValue();
              PrintWriter toPeer = new PrintWriter(heartbeatSocket.getOutputStream(), true);
              toPeer.println("Alive");
            }
          }
          Thread.sleep(5000);
        }
      } catch (IOException | InterruptedException e) {
        e.printStackTrace();
        Thread.currentThread().interrupt();
      }
    }).start();
  }
}
